This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## This app required the project "Pokerstats backend".
You can find it in this same Git.
Launch Pokerstats backend (see README.md of Pokerstats backend for more information about launching the back) 
in local on the port 5000 then launch this one

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
