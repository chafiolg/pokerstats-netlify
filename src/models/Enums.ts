export enum Finality {
  Win = "Win",
  Lose = "Lose",
  Finish = "Finish"
}

export enum Symbol {
  Heart = "heart",
  Diamond = "diamond",
  Spade = "spade",
  Club = "club"
}

export enum CardValue {
  none = "0",
  two = "2",
  three = "3",
  foor = "4",
  five = "5",
  six = "6",
  seven = "7",
  height = "8",
  nine = "9",
  ten = "10",
  J = "J",
  Q = "Q",
  K = "K",
  A = "A"
}
