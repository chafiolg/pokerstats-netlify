import CardManager from "./CardManager";

export default class TableManager {
  numberActivateCard: number;
  cardsTable: CardManager[];

  constructor() {
    this.numberActivateCard = 5;
    this.cardsTable = [new CardManager(), new CardManager(), new CardManager(), new CardManager(), new CardManager()];
  }
}
