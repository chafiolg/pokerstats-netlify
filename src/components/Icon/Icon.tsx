import React, { FunctionComponent, useRef } from "react";
import cn from "classnames";

import "./Icon.scss";

type AppProps = {
  tooltipGestion: (e: any) => void;
  selected: number; //-1 : unselectable; 0 : ok; 1 : alreadySelected
  value: string;
};

const Icon: FunctionComponent<AppProps> = (props) => {
  const getContent = () => {
    let content;
    let picture;

    const value = props.value;
    //cas des symboles (heart diamond spade club)
    if (!Number.isInteger(value) && value.length > 1 && value !== "interrogation") {
      picture = require("../../assets/pictures/" + value + ".png");
      content = <img src={picture} id="icon_img" alt="Icon" />;
    }
    //cas de toutes les valeurs de entre 2 et A
    else if (Number.isInteger(value) || value.length === 1) {
      content = <h2 className="iconContent"> {value} </h2>;
    }
    //cas d'interrogation
    else {
      picture = require("../../assets/pictures/interrogation.png");
      content = <img src={picture} id="icon_img" alt="Icon" />;
    }
    return content;
  };

  const clickIcon = (e: any) => {
    props.tooltipGestion(e.currentTarget.value);
  };

  const icon = useRef(null);
  return (
    <div ref={icon}>
      <button
        id="icon_component"
        className={cn("TooltipIcon", { selected: props.selected === 1 }, { unselectable: props.selected === -1 })}
        value={props.value}
        onClick={clickIcon}
        disabled={props.selected === -1}
      >
        {getContent()}
      </button>
    </div>
  );
};

export default Icon;
