import React, { FunctionComponent, useState } from "react";
import Main from "../Main/Main";
import DataPlayerColumn from "../DataPlayerColumn/DataPlayerColumn";
import Game from "../../models/Game";
import "./LogicBody.scss";
import Tooltip from "../Tooltip/Tooltip";
import CardManager from "../../models/CardManager";

export type AppProps = {
  game: Game;
};

export type ActivatePlayer = {
  [id: number]: boolean;
};
export type SetActivePlayer = (id: number, actif?: boolean) => void;
export type NewDisplayedCard = { position: PositionCard; newCard: CardManager };
export type PositionCard = { element: string; indexPlayer: number; position: number };

const useActivatePlayers = (): [ActivatePlayer, SetActivePlayer] => {
  const [activatedPlayerIds, _setActivedPlayerIds] = useState<ActivatePlayer>({});

  const setActivedPlayerIds = (id: number, actif: boolean = true) => {
    _setActivedPlayerIds({
      ...activatedPlayerIds,
      [id]: actif
    });
  };

  return [activatedPlayerIds, setActivedPlayerIds];
};

const LogicBody: FunctionComponent<AppProps> = ({ game }) => {
  const [activatedPlayerIds, setActivedPlayerIds] = useActivatePlayers();
  const [statisticDisplays, setStatisticDisplays] = useState<string[]>([]);
  const [tooltipDisplay, setTooltipDisplay] = useState<boolean>(false);
  const [cardComponent, setCardComponent] = useState<PositionCard>({ element: "none", indexPlayer: -1, position: 0 });
  const [cardObject, setCardObject] = useState<CardManager>(new CardManager());
  const openTooltip = (cardComponent: PositionCard) => {
    setTooltipDisplay(true);
    setCardComponent(cardComponent);
  };

  const updateCard = (card: CardManager) => {
    setCardObject(card);
    game.updateCardInGame({ position: cardComponent, newCard: card });
  };

  const closeTooltip = () => {
    setTooltipDisplay(false);
  };

  const newDisplayedCard: NewDisplayedCard = { position: cardComponent, newCard: cardObject };
  return (
    <div className="App-MainAndDataPlayer">
      <DataPlayerColumn
        players={game.players.slice(0, 4)}
        activatedPlayerIds={activatedPlayerIds}
        statisticDisplays={statisticDisplays.slice(0, 4)}
      />
      <div>
        <Main
          game={game}
          activatedPlayerIds={activatedPlayerIds}
          setActivedPlayerIds={setActivedPlayerIds}
          openTooltip={openTooltip}
          newDisplayedCard={newDisplayedCard}
          setStatisticDisplay={setStatisticDisplays}
        />
        {tooltipDisplay && (
          <Tooltip unavailableCards={game.pickedCards()} updateCard={updateCard} closeTooltip={closeTooltip} />
        )}
      </div>
      <DataPlayerColumn
        players={game.players.slice(4, 8).reverse()}
        activatedPlayerIds={activatedPlayerIds}
        statisticDisplays={statisticDisplays.slice(4, 8).reverse()}
      />
    </div>
  );
};

export default LogicBody;
