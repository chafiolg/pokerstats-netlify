import React, { FunctionComponent, useState, useEffect } from "react";
import Icon from "../Icon/Icon";
import Card from "../../models/CardManager";
import { Symbol, CardValue } from "../../models/Enums";
import "./Tooltip.scss";
import CardManager from "../../models/CardManager";

type AppProps = {
  unavailableCards: CardManager[];
  closeTooltip: () => void;
  updateCard: (card: CardManager) => void;
};
const Tooltip: FunctionComponent<AppProps> = (props) => {
  const [valueSelected, setValueSelected] = useState<string>();
  const [symbolSelected, setSymbolSelected] = useState<string>();
  const tooltipGestion = (value: string) => {
    if (value === "cross") {
      props.closeTooltip();
      return;
    } else if (value === "interrogation") {
      updateValueCard(true);
      return;
    }
    setNewStates(value);
  };

  useEffect(() => {
    if (valueSelected !== undefined && symbolSelected !== undefined) updateValueCard();
  }, [valueSelected, symbolSelected]);

  const updateValueCard = (interrogation: boolean = false): void => {
    if (interrogation) {
      props.updateCard(new CardManager());
    } else {
      props.updateCard(new CardManager(symbolSelected, valueSelected));
    }
    props.closeTooltip();
  };

  const setNewStates = (iconValue: string) => {
    //symbol case
    if (isNaN(parseInt(iconValue)) && iconValue.length > 1) {
      iconValue === symbolSelected ? setSymbolSelected(undefined) : setSymbolSelected(iconValue);
      //value case
    } else {
      iconValue === valueSelected ? setValueSelected(undefined) : setValueSelected(iconValue);
    }
  };

  const checkAvailability = (icon: string, symbol: boolean): number => {
    if (symbol) {
      if (valueSelected === undefined) {
        return 0;
      } else {
        return props.unavailableCards.findIndex((element) => element.equals(new Card(icon, valueSelected))) === -1
          ? 0
          : -1;
      }
    } else {
      if (symbolSelected === undefined) {
        return 0;
      } else {
        return props.unavailableCards.findIndex((element) => element.equals(new Card(symbolSelected, icon))) === -1
          ? 0
          : -1;
      }
    }
  };

  const cardValues = Object.values(CardValue).slice(1);
  return (
    <div className="Tooltip">
      <Icon value="interrogation" tooltipGestion={tooltipGestion} selected={0} />
      <div className="marge"></div>
      {Object.values(Symbol).map((symbol) => {
        return (
          <Icon
            value={symbol}
            tooltipGestion={tooltipGestion}
            selected={symbol === symbolSelected ? 1 : checkAvailability(symbol, true)}
          />
        );
      })}
      <div className="marge"></div>
      {cardValues.map((cardValue) => {
        return (
          <Icon
            value={cardValue}
            tooltipGestion={tooltipGestion}
            selected={cardValue === valueSelected ? 1 : checkAvailability(cardValue, false)}
          />
        );
      })}
      <div className="marge"></div>
      <Icon value="cross" tooltipGestion={tooltipGestion} selected={0} />
    </div>
  );
};

export default Tooltip;
