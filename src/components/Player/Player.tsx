import React, { FunctionComponent, createRef } from "react";
import cn from "classnames";
import PlayerC from "../../models/PlayerManager";
import logoPlus from "../../assets/pictures/activ.png";
import logoMoins from "../../assets/pictures/passiv.png";
import { ActivatePlayer, SetActivePlayer, PositionCard, NewDisplayedCard } from "../LogicBody/LogicBody";
import "./Player.scss";
import Card from "../Card/Card";

type AppProps = {
  player: PlayerC;
  position: object;
  activatedPlayerIds: ActivatePlayer;
  setActivedPlayerIds: SetActivePlayer;
  openTooltip: (cardComponent: PositionCard) => void;
  newDisplayedCard: NewDisplayedCard;
};

const Player: FunctionComponent<AppProps> = (props) => {
  let container = createRef<HTMLDivElement>();

  const toggleActivation = () => {
    props.setActivedPlayerIds(props.player.id, !props.activatedPlayerIds[props.player.id]);
    props.player.activ = !props.activatedPlayerIds[props.player.id];
  };

  const activ = props.activatedPlayerIds[props.player.id];
  const img = activ ? logoMoins : logoPlus;

  return (
    <div className="playerDiv">
      <div ref={container} className={cn("Player", { activate: activ })} style={props.position}>
        <div className="row">
          <h1 className="playerName col"> Player {props.player.id} </h1>
          <div className="thumbnail">
            <img className="img-responsive iconPlayer" onClick={toggleActivation} src={img} alt="activ/passiv" />
          </div>
        </div>
        <div className="row justify-content-center">
          <Card
            openTooltip={props.openTooltip}
            position={{ element: "Player", indexPlayer: props.player.id, position: 0 }}
            newDisplayedCard={props.newDisplayedCard}
            activate={activ}
          />
          <Card
            openTooltip={props.openTooltip}
            position={{ element: "Player", indexPlayer: props.player.id, position: 1 }}
            newDisplayedCard={props.newDisplayedCard}
            activate={activ}
          />
        </div>
      </div>
    </div>
  );
};
export default Player;
